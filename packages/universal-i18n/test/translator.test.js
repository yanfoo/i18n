import { 
   DEFAULT_NAMESPACE,
   DEFAULT_KEY_SEPARATOR, 
   DEFAULT_LOCALE, 
   DEFAULT_TOKEN_PATTERN,
} from '../src/const';

import Translator from '../src/translator';




describe('Testing Translator', () => {


   describe('Testing constructor', () => {

      it('should define interface', () => {
         const t = new Translator();

         expect(t.defaultLocale).toBe(DEFAULT_LOCALE);
         expect(t.defaultNamespace).toBe(DEFAULT_NAMESPACE);
         expect(t.tokenPattern).toBe(DEFAULT_TOKEN_PATTERN);
         expect(t.keySeparator).toBe(DEFAULT_KEY_SEPARATOR);
      });

      it('should throw on invalid defaultLocale', () => {
         [
            '', 'foo', -1, 0, 1,
            () => {}, {}, [], /./, new Date()
         ].forEach(defaultLocale => expect(() => new Translator({ defaultLocale })).toThrow());
      });

      it('should throw on invalid defaultNamespace', () => {
         [
            '', -1, 0, 1,
            () => {}, {}, [], /./, new Date()
         ].forEach(defaultNamespace => expect(() => new Translator({ defaultNamespace })).toThrow());
      });

      it('should throw on invalid keySeparator', () => {
         [
            '', -1, 0, 1,
            () => {}, {}, [], /./, new Date()
         ].forEach(keySeparator => expect(() => new Translator({ keySeparator })).toThrow());
      });

      it('should throw on invalid keySeparator', () => {
         [
            '', 'foo', -1, 0, 1,
            () => {}, {}, [], new Date()
         ].forEach(tokenPattern => expect(() => new Translator({ tokenPattern })).toThrow());
      });

   });


   describe('Testing translation', () => {

      const t = new Translator({
         defaultLocale: 'en',
         defaultNamespace: 'test'
      });

      t.addMessages('en', 'test', {
         "family": {
            "unit": "family",
            "children": {
               "zero": "no children",
               "one": {
                  "f": "daughter",
                  "m": "son",
                  "n": "child"
               },
               "other": {
                  "f": "{{n}} daughters",
                  "m": "{{n}} sons",
                  "n": "{{n}} children"
               }
            }
         }
      });

      t.addMessages('en', 'other', {
         "family": {
            "unit": "extended family",
         }
      });

      t.addMessages('fr', 'test', {
         "family": {
            "unit": "famille",
            "children": {
               "zero": "aucun enfant",
               "one": {
                  "f": "fille",
                  "m": "fils",
                  "n": "enfant"
               },
               "other": {
                  "f": "{{n}} filles",
                  "m": "{{n}} fils",
                  "n": "{{n}} enfants"
               }
            }
         }
      });


      it('should translate', () => {
         expect(t.translate('family.unit')).toBe('family');
         expect(t.translate('family.unit', { locale: 'fr' })).toBe('famille');
      });

      it('should translate (default locale)', () => {
         expect(t.translate('family.unit', { locale: 'zz' })).toBe('family');
      });

      it('should not translate missing message', () => {
         expect(t.translate('missing.message')).toBe('?test:missing.message');
      });

      it('should use namespace', () => {
         expect(t.translate('other:family.unit')).toBe('extended family');
      });


   });


});