import { 
   NAMESPACE_SEPARATOR,
   
   DEFAULT_NAMESPACE,
   DEFAULT_KEY_SEPARATOR, 
   DEFAULT_LOCALE, 
   DEFAULT_TOKEN_PATTERN,
   
   PLURAL_OTHER,

   GENDER_MALE,
   GENDER_FEMALE,
   GENDER_NEUTRAL
} from './const';
import plurals from './plurals';



const TOKEN_FORMAT_SEPARATOR = '|';
const TOKEN_DATA_SEPARATOR = '.';

const $MESSAGES = Symbol('$messages');
const $LOCALE_LOOKUP = Symbol('$localeLookup');
const $ALIAS_LOOKUP = Symbol('$aliasLookup');
const $FORMATTERS = Symbol('$formatters');



const defaultCardinality = () => PLURAL_OTHER;

const getOption = (t, k, opts, type) => {
   if ((opts?.[k] ?? null) !== null) {
      const isValidType = type === String 
         ? typeof opts[k] === 'string' 
         : opts[k] instanceof type
      ;

      if (!isValidType) {
         throw new TypeError(`Invalid ${k} value`);
      }
      t[k] = opts[k];
   }
};

const checkLanguage = locale => {
   if (!locale || (typeof locale !== 'string')) {
      throw new TypeError(`Invalid locale "${locale}""`);
   }

   const langCode = locale.split('-', 1).shift();

   if (!(langCode in plurals)) {
      throw new Error(`Unrecognized language code "${langCode}"`);
   }
};

const isValidGender = gender => (gender === GENDER_NEUTRAL) || (gender === GENDER_FEMALE) || (gender === GENDER_MALE);




class Translator {

   tokenPattern = DEFAULT_TOKEN_PATTERN;
   keySeparator = DEFAULT_KEY_SEPARATOR;
   defaultNamespace = DEFAULT_NAMESPACE;
   defaultLocale = DEFAULT_LOCALE;

   constructor(options) {
      getOption(this, 'tokenPattern', options, RegExp);
      getOption(this, 'keySeparator', options, String);
      getOption(this, 'defaultLocale', options, String);
      getOption(this, 'defaultNamespace', options, String);

      checkLanguage(this.defaultLocale);
      
      if (!this.keySeparator) {
         throw new Error('Invalid empty token key separator');
      }
      if (!this.defaultNamespace) {
         throw new Error('Invalid empty default namespace');
      }

      this[$MESSAGES] = new Map();
      this[$LOCALE_LOOKUP] = new Map();  // locale->alias
      this[$ALIAS_LOOKUP] = new Map();   // alias->[locale, ...]
      this[$FORMATTERS] = new Map();
   }


   addFormatter(formatter) {
      if ((typeof formatter !== 'function') || !formatter?.name) {
         throw new TypeError('Formatter must be a named function');
      } else if (this[$FORMATTERS].has(formatter.name)) {
         throw new Error(`Formatter ${formatter.name} alrady exists!`);
      }

      this[$FORMATTERS].set(formatter.name, formatter);
   }

   addMessages(locale, namespace, messages) {
      checkLanguage(locale);

      if (!this[$MESSAGES].has(locale)) {
         this[$MESSAGES].set(locale, new Map());
      }

      namespace = namespace || this.defaultNamespace;

      this[$MESSAGES].get(locale).set(namespace, messages);

      return true;
   }

   hasAloas(locale) {
      return this[$LOCALE_LOOKUP].has(locale);
   }

   getAlias(locale) {
      return this[$LOCALE_LOOKUP].get(locale) ?? null;
   }

   getAliasLookup(alias) {
      return new Set(this[$ALIAS_LOOKUP].get(alias) ?? []);
   }

   setAlias(aliases) {
      let added = false;

      for (let [ alias, locales ] of aliases) {
         if (!alias || (typeof alias !== 'string')) {
            throw new Error(`Invalid alias ${alias}`);
         }

         if (typeof locales === 'string') {
            locales = [locales];
         }

         for (const locale of locales) {
            checkLanguage(locale);

            if (!this[$LOCALE_LOOKUP].has(locale)) {
               added = true;
            } else if (this[$LOCALE_LOOKUP].get(locale) !== alias) {
               throw new Error(`Cannot set alias ${alias} of ${locale} because it is already set as an alias of ${this[$LOCALE_LOOKUP].get(locale)}`);
            } else if (this[$LOCALE_LOOKUP].get(alias) !== locale) {
               throw new Error(`Cyclical dependency found between ${alias} and ${locale}`);
            }

            this[$LOCALE_LOOKUP].set(locale, alias);
         }

         this[$ALIAS_LOOKUP].set(alias, new Set(locales));
      }

      return added;
   }

   translate(key, options) {
      const keyParts = key.split(NAMESPACE_SEPARATOR, 2);
      const namespace = (keyParts.length > 1 ? keyParts[0] : null) || this.defaultNamespace;
      const keyPath = keyParts[keyParts.length - 1]?.split?.(this.keySeparator) ?? [];
      const locale = options?.locale || this.defaultLocale;
      const hasPlural = !isNaN(options?.plurality);
      const pluralKey = hasPlural ? (plurals[locale]?.cardinal ?? plurals[this.defaultLocale].cardinal ?? defaultCardinality)(options.plurality) : PLURAL_OTHER;
      const hasGender = isValidGender(options?.gender);
      const genderKey = hasGender ? options.gender : GENDER_NEUTRAL;
      
      let message = this[$MESSAGES].get(locale)?.get(namespace) 
         ?? (this.hasAloas(locale) ? this[$MESSAGES].get(this.getAlias(locale))?.get(namespace) : null)     // alias?
         ?? (locale !== this.defaultLocale ? this[$MESSAGES].get(this.defaultLocale)?.get(namespace) : null) // default locale (if different from locale)
         ?? {};   // nothing

      // walk until we find a string
      while (message && (typeof message !== 'string')) {
         message = message?.[keyPath.shift()];
      }

      if (message?.[pluralKey]) {
         message = message[pluralKey];
      }
      if (message?.[genderKey]) {
         message = message[genderKey];
      }

      if (typeof message === 'string') {
         const messageData = options?.data || {};
         const messageFormat = options?.format || {};
         const messageFormatOptions = { locale };
         
         if (hasPlural) {
            messageFormatOptions.plurality = options.plurality;
         }
         if (hasGender) {
            messageFormatOptions.gender = options.gender;
         }

         return message.replace(this.tokenPattern, (_m, token) => {
            const [ tokenPath, ...formatters ] = token.split(TOKEN_FORMAT_SEPARATOR);
            const tokenValue = tokenPath.split(TOKEN_DATA_SEPARATOR).reduce((value, prop) => value?.[prop] ?? null, messageData) ?? '';
            
            return formatters.length 
               ? formatters.map(formatter => this[$FORMATTERS].get(formatter) ?? (x => x)).reduce((value, format) => format(value, { ...messageFormatOptions, ...messageFormat[format.name] }), tokenValue) 
               : tokenValue;
         });
      } else {
         if (message === null || message === void 0) {
            console.warn(`[i18n.translate] missing message ${namespace}:${key}`);
         } else {
            console.warn(`[i18n.translate] expected ${namespace}:${key} to match a string, ${message !== null ? typeof message : 'null'} found`);
         }

         return `?${namespace}:${key}`;
      }
   }

}


export default Translator;