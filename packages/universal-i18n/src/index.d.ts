

declare type TranslatorOptions = {

   /**
    * The expression to replace tokens in the message.
    */
   tokenPattern: RegExp;

   /**
    * The string to indicate where to split the key into a key path.
    */
   keySeparator: String;
 
   /**
    * The default namespace to use when none is provided
    */
   defaultNamespace: String;

   /**
    * The default locale to use when the given locale can't found, will default to this one before failing.
    */
   defaultLocale: String;

}



declare type TranslateOptions = {

   /**
    * Optionally defines the gender in the message.
    * Possible values include : 'm' for male, 'f' for female, and 'n' for undefined gender
    */
   gender: String;

   /**
    * The locale overeride
    */
   locale: String;

   /**
    * Optionally defines the plurality value, a numeric value representing a quantity
    */
   plurality: Number;

}


declare type TMmessage = string | TMessages;
declare interface TMessages extends Record<string,TMmessage> { }



declare interface TranslatorPlugin { 
   init(translator:Translator): Boolean
}


export class Translator {

   /**
    * The expression to replace tokens in the message.
    */
   tokenPattern: RegExp;

   /**
    * The string to indicate where to split the key into a key path.
    */
   keySeparator: string;

   /**
    * The default namespace for when no namespace is provided
    */
   defaultNamespace: string;

   /**
    * The default locale to use when the given locale can't found, will default to this one before failing.
    */
   defaultLocale: string;


   /**
    * Construct a new Translator instance
    * @param options     optionally specify overrides
    */
   constructor(options:TranslatorOptions);



   /**
    * Add a formatter. The formatter must have a name, it cannot be an anonymous function!
    * @param formatter 
    */
   addFormatter(formatter: { (value:string, options:any): string }): void;

   /**
    * Add messages to this translator. The messages will be merged into the
    * specified namespace, or into the default namespace if none is provided.
    * 
    * The locale should be a valid IETF language tag.
    * 
    * Usage:
    *    
    *    peopleMessages = {
    *       "family": "family",
    *       "children": {
    *          "zero": "no children",
    *          "one": {
    *             "f": "daughter",
    *             "m": "son",
    *             "n": "child"
    *          },
    *          "other": {
    *             "f": "{{n}} daughters",
    *             "m": "{{n}} sons",
    *             "n": "{{n}} children"
    *          }
    *       }
    *    }
    *    globalDictionary = {
    *       ...
    *    }
    * 
    *    t.addMessages('en-US', 'people', peopleMessages);
    *    t.addMessages('en-US', null, globalDictionary);
    * 
    * 
    * @param locale        the locale of the messages being added
    * @param namespace     the namespace of the messages being added
    * @param messages      the messages
    */
   addMessages(locale:string, namespace:string, messages:TMessages): boolean;

   /**
    * Check whether a given locale has an alias
    * @param locale 
    */
   hasAloas(locale:string): Boolean;

   /**
    * Get the alias for the specified locale, or null if the locale has no alias
    * @param locale 
    */
   getAlias(locale:string): string | null;

   /**
    * Check if the given alias exists, and return all the locales it aliases.
    * Returns an empty list if the alias does not exist.
    * @param alias 
    * @param locale 
    */
   getAliasLookup(alias:string): Iterable<string>;

   /**
    * Set a few locale aliases. This method is usefull when trying to check for other
    * locales when a given message is not found with the specified locale.
    * Returns true if at least one alias has been set.
    * 
    * Usage:
    * 
    *    const t = new Translator({ defaultLocale: 'en' });
    *    t.setAlias({'fr': ['fr-FR', 'fr-CA']});
    *    t.addMessages('fr', null, ...)
    *    t.addMessages('fr-CA', null, ...)
    * 
    *    t.translate('some.message', { locale: 'fr-FR' })  // will try 'fr-FR', 'fr', then 'en'
    * 
    * @param alias 
    * @param locales 
    */
   setAlias(aliases:Record<string, string | Iterable<string>>): Boolean;

   /**
    * Translate the given key into a message
    * 
    * Usage:
    * 
    *    translate('people:family');                                    // ex: "family"
    *    translate('people:children', { gender: 'f' })                  // ex: 'daughters'
    *    translate('people:children', { gender: 'm', plurality:1 })     // ex: 'son'
    *    translate('people:children', { plurality:4, data: { n: 4 } })  // ex: '4 children'
    * 
    * 
    * 
    * @param key           the key of the message being translated
    * @param options       the translation option
    */
   translate(key:string, options:TranslateOptions): string;

   /**
    * Use a plugin, returns true if the plugin was successfully added, or
    * false otherwise.
    * @param plugin 
    */
   use(plugin:TranslatorPlugin): Boolean

}

