/**

Plural file generated from CLDR-DATA (http://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html)

See ../build_plurals.js for more information.

**/

import { PLURAL_ZERO, PLURAL_ONE, PLURAL_TWO, PLURAL_FEW, PLURAL_MANY, PLURAL_OTHER } from './const';

const _f = p => Math.floor((p+"").split(".")[1])||0;
const _i = n => Math.floor(n)||0;
const _n = p => Math.abs(p)||0;
const _t = n => Math.floor((n+"").split(".")[1])||0;
const _v = p => ((p+"").split(".")[1]||"").length;
const car1 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car2 = p => { const n = _n(p); return (n >= 0 && n <= 1) ? PLURAL_ONE : PLURAL_OTHER; };
const car3 = p => { const n = _n(p), i = _i(n); return i === 0 || n === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car4 = p => { const n = _n(p), n100 = n % 100; return n === 0 ? PLURAL_ZERO : n === 1 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : (n100 >= 3 && n100 <= 10) ? PLURAL_FEW : (n100 >= 11 && n100 <= 99) ? PLURAL_MANY : PLURAL_OTHER; };
const car5 = p => { const n = _n(p), i = _i(n), v = _v(p); return i === 1 && v === 0 ? PLURAL_ONE : PLURAL_OTHER; };
const car6 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n10 === 1 && n100 !== 11 ? PLURAL_ONE : (n10 >= 2 && n10 <= 4) && !(n100 >= 12 && n100 <= 14) ? PLURAL_FEW : n10 === 0 || (n10 >= 5 && n10 <= 9) || (n100 >= 11 && n100 <= 14) ? PLURAL_MANY : PLURAL_OTHER; };
const car7 = () => { return PLURAL_OTHER; };
const car8 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100, n1000000 = n % 1000000; return n10 === 1 && n100 !== 11 && n100 !== 71 && n100 !== 91 ? PLURAL_ONE : n10 === 2 && n100 !== 12 && n100 !== 72 && n100 !== 92 ? PLURAL_TWO : (n10 >= 3 && n10 <= 4) || n10 === 9 && !(n100 >= 10 && n100 <= 19) && !(n100 >= 70 && n100 <= 79) && !(n100 >= 90 && n100 <= 99) ? PLURAL_FEW : n !== 0 && n1000000 === 0 ? PLURAL_MANY : PLURAL_OTHER; };
const car9 = p => { const n = _n(p), i = _i(n), v = _v(p), f = _f(p), i10 = i % 10, i100 = i % 100, f10 = f % 10, f100 = f % 100; return v === 0 && i10 === 1 && i100 !== 11 || f10 === 1 && f100 !== 11 ? PLURAL_ONE : v === 0 && (i10 >= 2 && i10 <= 4) && !(i100 >= 12 && i100 <= 14) || (f10 >= 2 && f10 <= 4) && !(f100 >= 12 && f100 <= 14) ? PLURAL_FEW : PLURAL_OTHER; };
const car10 = p => { const n = _n(p), i = _i(n), v = _v(p), f = _f(p), i10 = i % 10, f10 = f % 10; return v === 0 && i === 1 || i === 2 || i === 3 || v === 0 && i10 !== 4 && i10 !== 6 && i10 !== 9 || v !== 0 && f10 !== 4 && f10 !== 6 && f10 !== 9 ? PLURAL_ONE : PLURAL_OTHER; };
const car11 = p => { const n = _n(p), i = _i(n), v = _v(p); return i === 1 && v === 0 ? PLURAL_ONE : (i >= 2 && i <= 4) && v === 0 ? PLURAL_FEW : v !== 0 ? PLURAL_MANY : PLURAL_OTHER; };
const car12 = p => { const n = _n(p); return n === 0 ? PLURAL_ZERO : n === 1 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : n === 3 ? PLURAL_FEW : n === 6 ? PLURAL_MANY : PLURAL_OTHER; };
const car13 = p => { const n = _n(p), i = _i(n), t = _t(n); return n === 1 || t !== 0 && i === 0 || i === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car14 = p => { const n = _n(p), i = _i(n), v = _v(p), f = _f(p), i100 = i % 100, f100 = f % 100; return v === 0 && i100 === 1 || f100 === 1 ? PLURAL_ONE : v === 0 && i100 === 2 || f100 === 2 ? PLURAL_TWO : v === 0 && (i100 >= 3 && i100 <= 4) || (f100 >= 3 && f100 <= 4) ? PLURAL_FEW : PLURAL_OTHER; };
const car15 = p => { const n = _n(p), i = _i(n); return i === 0 || i === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car16 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : (n >= 3 && n <= 6) ? PLURAL_FEW : (n >= 7 && n <= 10) ? PLURAL_MANY : PLURAL_OTHER; };
const car17 = p => { const n = _n(p); return n === 1 || n === 11 ? PLURAL_ONE : n === 2 || n === 12 ? PLURAL_TWO : (n >= 3 && n <= 10) || (n >= 13 && n <= 19) ? PLURAL_FEW : PLURAL_OTHER; };
const car18 = p => { const n = _n(p), i = _i(n), v = _v(p), i10 = i % 10, i100 = i % 100; return v === 0 && i10 === 1 ? PLURAL_ONE : v === 0 && i10 === 2 ? PLURAL_TWO : v === 0 && i100 === 0 || i100 === 20 || i100 === 40 || i100 === 60 || i100 === 80 ? PLURAL_FEW : v !== 0 ? PLURAL_MANY : PLURAL_OTHER; };
const car19 = p => { const n = _n(p), i = _i(n), v = _v(p), n10 = n % 10; return i === 1 && v === 0 ? PLURAL_ONE : i === 2 && v === 0 ? PLURAL_TWO : v === 0 && !(n >= 0 && n <= 10) && n10 === 0 ? PLURAL_MANY : PLURAL_OTHER; };
const car20 = p => { const n = _n(p), i = _i(n), t = _t(n), i10 = i % 10, i100 = i % 100; return t === 0 && i10 === 1 && i100 !== 11 || t !== 0 ? PLURAL_ONE : PLURAL_OTHER; };
const car21 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : PLURAL_OTHER; };
const car22 = p => { const n = _n(p); return n === 0 ? PLURAL_ZERO : n === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car23 = p => { const n = _n(p), n100 = n % 100, n1000 = n % 1000, n100000 = n % 100000, n1000000 = n % 1000000; return n === 0 ? PLURAL_ZERO : n === 1 ? PLURAL_ONE : n100 === 2 || n100 === 22 || n100 === 42 || n100 === 62 || n100 === 82 || n1000 === 0 && (n100000 >= 1000 && n100000 <= 20000) || n100000 === 40000 || n100000 === 60000 || n100000 === 80000 || n !== 0 && n1000000 === 100000 ? PLURAL_TWO : n100 === 3 || n100 === 23 || n100 === 43 || n100 === 63 || n100 === 83 ? PLURAL_FEW : n !== 1 && n100 === 1 || n100 === 21 || n100 === 41 || n100 === 61 || n100 === 81 ? PLURAL_MANY : PLURAL_OTHER; };
const car24 = p => { const n = _n(p), i = _i(n); return n === 0 ? PLURAL_ZERO : i === 0 || i === 1 && n !== 0 ? PLURAL_ONE : PLURAL_OTHER; };
const car25 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100, f = _f(p); return n10 === 1 && !(n100 >= 11 && n100 <= 19) ? PLURAL_ONE : (n10 >= 2 && n10 <= 9) && !(n100 >= 11 && n100 <= 19) ? PLURAL_FEW : f !== 0 ? PLURAL_MANY : PLURAL_OTHER; };
const car26 = p => { const v = _v(p), f = _f(p), n = _n(p), n10 = n % 10, n100 = n % 100, f100 = f % 100, f10 = f % 10; return n10 === 0 || (n100 >= 11 && n100 <= 19) || v === 2 && (f100 >= 11 && f100 <= 19) ? PLURAL_ZERO : n10 === 1 && n100 !== 11 || v === 2 && f10 === 1 && f100 !== 11 || v !== 2 && f10 === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car27 = p => { const n = _n(p), i = _i(n), v = _v(p), f = _f(p), i10 = i % 10, i100 = i % 100, f10 = f % 10, f100 = f % 100; return v === 0 && i10 === 1 && i100 !== 11 || f10 === 1 && f100 !== 11 ? PLURAL_ONE : PLURAL_OTHER; };
const car28 = p => { const n = _n(p), i = _i(n), v = _v(p), n100 = n % 100; return i === 1 && v === 0 ? PLURAL_ONE : v !== 0 || n === 0 || (n100 >= 2 && n100 <= 19) ? PLURAL_FEW : PLURAL_OTHER; };
const car29 = p => { const n = _n(p), n100 = n % 100; return n === 1 ? PLURAL_ONE : n === 0 || (n100 >= 2 && n100 <= 10) ? PLURAL_FEW : (n100 >= 11 && n100 <= 19) ? PLURAL_MANY : PLURAL_OTHER; };
const car30 = p => { const n = _n(p), i = _i(n), v = _v(p), i10 = i % 10, i100 = i % 100; return i === 1 && v === 0 ? PLURAL_ONE : v === 0 && (i10 >= 2 && i10 <= 4) && !(i100 >= 12 && i100 <= 14) ? PLURAL_FEW : v === 0 && i !== 1 && (i10 >= 0 && i10 <= 1) || v === 0 && (i10 >= 5 && i10 <= 9) || v === 0 && (i100 >= 12 && i100 <= 14) ? PLURAL_MANY : PLURAL_OTHER; };
const car31 = p => { const n = _n(p), i = _i(n); return (i >= 0 && i <= 1) ? PLURAL_ONE : PLURAL_OTHER; };
const car32 = p => { const n = _n(p), i = _i(n), v = _v(p), i10 = i % 10, i100 = i % 100; return v === 0 && i10 === 1 && i100 !== 11 ? PLURAL_ONE : v === 0 && (i10 >= 2 && i10 <= 4) && !(i100 >= 12 && i100 <= 14) ? PLURAL_FEW : v === 0 && i10 === 0 || v === 0 && (i10 >= 5 && i10 <= 9) || v === 0 && (i100 >= 11 && i100 <= 14) ? PLURAL_MANY : PLURAL_OTHER; };
const car33 = p => { const n = _n(p), i = _i(n); return i === 0 || n === 1 ? PLURAL_ONE : (n >= 2 && n <= 10) ? PLURAL_FEW : PLURAL_OTHER; };
const car34 = p => { const n = _n(p), i = _i(n), f = _f(p); return n === 0 || n === 1 || i === 0 && f === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const car35 = p => { const n = _n(p), i = _i(n), v = _v(p), i100 = i % 100; return v === 0 && i100 === 1 ? PLURAL_ONE : v === 0 && i100 === 2 ? PLURAL_TWO : v === 0 && (i100 >= 3 && i100 <= 4) || v !== 0 ? PLURAL_FEW : PLURAL_OTHER; };
const car36 = p => { const n = _n(p); return (n >= 0 && n <= 1) || (n >= 11 && n <= 99) ? PLURAL_ONE : PLURAL_OTHER; };
const ord1 = () => { return PLURAL_OTHER; };
const ord2 = p => { const n = _n(p); return n === 1 || n === 5 || n === 7 || n === 8 || n === 9 || n === 10 ? PLURAL_ONE : n === 2 || n === 3 ? PLURAL_TWO : n === 4 ? PLURAL_FEW : n === 6 ? PLURAL_MANY : PLURAL_OTHER; };
const ord3 = p => { const n = _n(p), i = _i(n), i10 = i % 10, i100 = i % 100, i1000 = i % 1000; return i10 === 1 || i10 === 2 || i10 === 5 || i10 === 7 || i10 === 8 || i100 === 20 || i100 === 50 || i100 === 70 || i100 === 80 ? PLURAL_ONE : i10 === 3 || i10 === 4 || i1000 === 100 || i1000 === 200 || i1000 === 300 || i1000 === 400 || i1000 === 500 || i1000 === 600 || i1000 === 700 || i1000 === 800 || i1000 === 900 ? PLURAL_FEW : i === 0 || i10 === 6 || i100 === 40 || i100 === 60 || i100 === 90 ? PLURAL_MANY : PLURAL_OTHER; };
const ord4 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n10 === 2 || n10 === 3 && n100 !== 12 && n100 !== 13 ? PLURAL_FEW : PLURAL_OTHER; };
const ord5 = p => { const n = _n(p); return n === 1 || n === 3 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : n === 4 ? PLURAL_FEW : PLURAL_OTHER; };
const ord6 = p => { const n = _n(p); return n === 0 || n === 7 || n === 8 || n === 9 ? PLURAL_ZERO : n === 1 ? PLURAL_ONE : n === 2 ? PLURAL_TWO : n === 3 || n === 4 ? PLURAL_FEW : n === 5 || n === 6 ? PLURAL_MANY : PLURAL_OTHER; };
const ord7 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n10 === 1 && n100 !== 11 ? PLURAL_ONE : n10 === 2 && n100 !== 12 ? PLURAL_TWO : n10 === 3 && n100 !== 13 ? PLURAL_FEW : PLURAL_OTHER; };
const ord8 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : PLURAL_OTHER; };
const ord9 = p => { const n = _n(p); return n === 1 || n === 11 ? PLURAL_ONE : n === 2 || n === 12 ? PLURAL_TWO : n === 3 || n === 13 ? PLURAL_FEW : PLURAL_OTHER; };
const ord10 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : n === 2 || n === 3 ? PLURAL_TWO : n === 4 ? PLURAL_FEW : n === 6 ? PLURAL_MANY : PLURAL_OTHER; };
const ord11 = p => { const n = _n(p); return n === 1 || n === 5 ? PLURAL_ONE : PLURAL_OTHER; };
const ord12 = p => { const n = _n(p); return n === 11 || n === 8 || n === 80 || n === 800 ? PLURAL_MANY : PLURAL_OTHER; };
const ord13 = p => { const n = _n(p), i = _i(n), i100 = i % 100; return i === 1 ? PLURAL_ONE : i === 0 || (i100 >= 2 && i100 <= 20) || i100 === 40 || i100 === 60 || i100 === 80 ? PLURAL_MANY : PLURAL_OTHER; };
const ord14 = p => { const n = _n(p), n10 = n % 10; return n10 === 6 || n10 === 9 || n10 === 0 && n !== 0 ? PLURAL_MANY : PLURAL_OTHER; };
const ord15 = p => { const n = _n(p), n100 = n % 100; return (n >= 1 && n <= 4) || (n100 >= 1 && n100 <= 4) || (n100 >= 21 && n100 <= 24) || (n100 >= 41 && n100 <= 44) || (n100 >= 61 && n100 <= 64) || (n100 >= 81 && n100 <= 84) ? PLURAL_ONE : n === 5 || n100 === 5 ? PLURAL_MANY : PLURAL_OTHER; };
const ord16 = p => { const n = _n(p), i = _i(n), i10 = i % 10, i100 = i % 100; return i10 === 1 && i100 !== 11 ? PLURAL_ONE : i10 === 2 && i100 !== 12 ? PLURAL_TWO : i10 === 7 || i10 === 8 && i100 !== 17 && i100 !== 18 ? PLURAL_MANY : PLURAL_OTHER; };
const ord17 = p => { const n = _n(p); return n === 1 ? PLURAL_ONE : n === 2 || n === 3 ? PLURAL_TWO : n === 4 ? PLURAL_FEW : PLURAL_OTHER; };
const ord18 = p => { const n = _n(p); return (n >= 1 && n <= 4) ? PLURAL_ONE : PLURAL_OTHER; };
const ord19 = p => { const n = _n(p); return n === 1 || n === 5 || (n >= 7 && n <= 9) ? PLURAL_ONE : n === 2 || n === 3 ? PLURAL_TWO : n === 4 ? PLURAL_FEW : n === 6 ? PLURAL_MANY : PLURAL_OTHER; };
const ord20 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n === 1 ? PLURAL_ONE : n10 === 4 && n100 !== 14 ? PLURAL_MANY : PLURAL_OTHER; };
const ord21 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n10 === 1 || n10 === 2 && n100 !== 11 && n100 !== 12 ? PLURAL_ONE : PLURAL_OTHER; };
const ord22 = p => { const n = _n(p), n10 = n % 10; return n10 === 6 || n10 === 9 || n === 10 ? PLURAL_FEW : PLURAL_OTHER; };
const ord23 = p => { const n = _n(p), n10 = n % 10, n100 = n % 100; return n10 === 3 && n100 !== 13 ? PLURAL_FEW : PLURAL_OTHER; };

export default {
  "af": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "ak": {
    "cardinal": car2
  },
  "am": {
    "cardinal": car3,
    "ordinal": ord1
  },
  "an": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "ar": {
    "cardinal": car4,
    "ordinal": ord1
  },
  "ars": {
    "cardinal": car4
  },
  "as": {
    "cardinal": car3,
    "ordinal": ord2
  },
  "asa": {
    "cardinal": car1
  },
  "ast": {
    "cardinal": car5
  },
  "az": {
    "cardinal": car1,
    "ordinal": ord3
  },
  "be": {
    "cardinal": car6,
    "ordinal": ord4
  },
  "bem": {
    "cardinal": car1
  },
  "bez": {
    "cardinal": car1
  },
  "bg": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "bho": {
    "cardinal": car2
  },
  "bm": {
    "cardinal": car7
  },
  "bn": {
    "cardinal": car3,
    "ordinal": ord2
  },
  "bo": {
    "cardinal": car7
  },
  "br": {
    "cardinal": car8
  },
  "brx": {
    "cardinal": car1
  },
  "bs": {
    "cardinal": car9,
    "ordinal": ord1
  },
  "ca": {
    "cardinal": car5,
    "ordinal": ord5
  },
  "ce": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "ceb": {
    "cardinal": car10
  },
  "cgg": {
    "cardinal": car1
  },
  "chr": {
    "cardinal": car1
  },
  "ckb": {
    "cardinal": car1
  },
  "cs": {
    "cardinal": car11,
    "ordinal": ord1
  },
  "cy": {
    "cardinal": car12,
    "ordinal": ord6
  },
  "da": {
    "cardinal": car13,
    "ordinal": ord1
  },
  "de": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "dsb": {
    "cardinal": car14,
    "ordinal": ord1
  },
  "dv": {
    "cardinal": car1
  },
  "dz": {
    "cardinal": car7
  },
  "ee": {
    "cardinal": car1
  },
  "el": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "en": {
    "cardinal": car5,
    "ordinal": ord7
  },
  "eo": {
    "cardinal": car1
  },
  "es": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "et": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "eu": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "fa": {
    "cardinal": car3,
    "ordinal": ord1
  },
  "ff": {
    "cardinal": car15
  },
  "fi": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "fil": {
    "cardinal": car10,
    "ordinal": ord8
  },
  "fo": {
    "cardinal": car1
  },
  "fr": {
    "cardinal": car15,
    "ordinal": ord8
  },
  "fur": {
    "cardinal": car1
  },
  "fy": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "ga": {
    "cardinal": car16,
    "ordinal": ord8
  },
  "gd": {
    "cardinal": car17,
    "ordinal": ord9
  },
  "gl": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "gsw": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "gu": {
    "cardinal": car3,
    "ordinal": ord10
  },
  "guw": {
    "cardinal": car2
  },
  "gv": {
    "cardinal": car18
  },
  "ha": {
    "cardinal": car1
  },
  "haw": {
    "cardinal": car1
  },
  "he": {
    "cardinal": car19,
    "ordinal": ord1
  },
  "hi": {
    "cardinal": car3,
    "ordinal": ord10
  },
  "hr": {
    "cardinal": car9,
    "ordinal": ord1
  },
  "hsb": {
    "cardinal": car14,
    "ordinal": ord1
  },
  "hu": {
    "cardinal": car1,
    "ordinal": ord11
  },
  "hy": {
    "cardinal": car15,
    "ordinal": ord8
  },
  "ia": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "id": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "ig": {
    "cardinal": car7
  },
  "ii": {
    "cardinal": car7
  },
  "in": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "io": {
    "cardinal": car5
  },
  "is": {
    "cardinal": car20,
    "ordinal": ord1
  },
  "it": {
    "cardinal": car5,
    "ordinal": ord12
  },
  "iu": {
    "cardinal": car21
  },
  "iw": {
    "cardinal": car19,
    "ordinal": ord1
  },
  "ja": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "jbo": {
    "cardinal": car7
  },
  "jgo": {
    "cardinal": car1
  },
  "ji": {
    "cardinal": car5
  },
  "jmc": {
    "cardinal": car1
  },
  "jv": {
    "cardinal": car7
  },
  "jw": {
    "cardinal": car7
  },
  "ka": {
    "cardinal": car1,
    "ordinal": ord13
  },
  "kab": {
    "cardinal": car15
  },
  "kaj": {
    "cardinal": car1
  },
  "kcg": {
    "cardinal": car1
  },
  "kde": {
    "cardinal": car7
  },
  "kea": {
    "cardinal": car7
  },
  "kk": {
    "cardinal": car1,
    "ordinal": ord14
  },
  "kkj": {
    "cardinal": car1
  },
  "kl": {
    "cardinal": car1
  },
  "km": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "kn": {
    "cardinal": car3,
    "ordinal": ord1
  },
  "ko": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "ks": {
    "cardinal": car1
  },
  "ksb": {
    "cardinal": car1
  },
  "ksh": {
    "cardinal": car22
  },
  "ku": {
    "cardinal": car1
  },
  "kw": {
    "cardinal": car23,
    "ordinal": ord15
  },
  "ky": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "lag": {
    "cardinal": car24
  },
  "lb": {
    "cardinal": car1
  },
  "lg": {
    "cardinal": car1
  },
  "lkt": {
    "cardinal": car7
  },
  "ln": {
    "cardinal": car2
  },
  "lo": {
    "cardinal": car7,
    "ordinal": ord8
  },
  "lt": {
    "cardinal": car25,
    "ordinal": ord1
  },
  "lv": {
    "cardinal": car26,
    "ordinal": ord1
  },
  "mas": {
    "cardinal": car1
  },
  "mg": {
    "cardinal": car2
  },
  "mgo": {
    "cardinal": car1
  },
  "mk": {
    "cardinal": car27,
    "ordinal": ord16
  },
  "ml": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "mn": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "mo": {
    "cardinal": car28,
    "ordinal": ord8
  },
  "mr": {
    "cardinal": car1,
    "ordinal": ord17
  },
  "ms": {
    "cardinal": car7,
    "ordinal": ord8
  },
  "mt": {
    "cardinal": car29
  },
  "my": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "nah": {
    "cardinal": car1
  },
  "naq": {
    "cardinal": car21
  },
  "nb": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "nd": {
    "cardinal": car1
  },
  "ne": {
    "cardinal": car1,
    "ordinal": ord18
  },
  "nl": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "nn": {
    "cardinal": car1
  },
  "nnh": {
    "cardinal": car1
  },
  "no": {
    "cardinal": car1
  },
  "nqo": {
    "cardinal": car7
  },
  "nr": {
    "cardinal": car1
  },
  "nso": {
    "cardinal": car2
  },
  "ny": {
    "cardinal": car1
  },
  "nyn": {
    "cardinal": car1
  },
  "om": {
    "cardinal": car1
  },
  "or": {
    "cardinal": car1,
    "ordinal": ord19
  },
  "os": {
    "cardinal": car1
  },
  "osa": {
    "cardinal": car7
  },
  "pa": {
    "cardinal": car2,
    "ordinal": ord1
  },
  "pap": {
    "cardinal": car1
  },
  "pl": {
    "cardinal": car30,
    "ordinal": ord1
  },
  "prg": {
    "cardinal": car26,
    "ordinal": ord1
  },
  "ps": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "pt": {
    "cardinal": car31,
    "ordinal": ord1
  },
  "pt-PT": {
    "cardinal": car5
  },
  "rm": {
    "cardinal": car1
  },
  "ro": {
    "cardinal": car28,
    "ordinal": ord8
  },
  "rof": {
    "cardinal": car1
  },
  "root": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "ru": {
    "cardinal": car32,
    "ordinal": ord1
  },
  "rwk": {
    "cardinal": car1
  },
  "sah": {
    "cardinal": car7
  },
  "saq": {
    "cardinal": car1
  },
  "sc": {
    "cardinal": car5,
    "ordinal": ord12
  },
  "scn": {
    "cardinal": car5,
    "ordinal": ord12
  },
  "sd": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "sdh": {
    "cardinal": car1
  },
  "se": {
    "cardinal": car21
  },
  "seh": {
    "cardinal": car1
  },
  "ses": {
    "cardinal": car7
  },
  "sg": {
    "cardinal": car7
  },
  "sh": {
    "cardinal": car9,
    "ordinal": ord1
  },
  "shi": {
    "cardinal": car33
  },
  "si": {
    "cardinal": car34,
    "ordinal": ord1
  },
  "sk": {
    "cardinal": car11,
    "ordinal": ord1
  },
  "sl": {
    "cardinal": car35,
    "ordinal": ord1
  },
  "sma": {
    "cardinal": car21
  },
  "smi": {
    "cardinal": car21
  },
  "smj": {
    "cardinal": car21
  },
  "smn": {
    "cardinal": car21
  },
  "sms": {
    "cardinal": car21
  },
  "sn": {
    "cardinal": car1
  },
  "so": {
    "cardinal": car1
  },
  "sq": {
    "cardinal": car1,
    "ordinal": ord20
  },
  "sr": {
    "cardinal": car9,
    "ordinal": ord1
  },
  "ss": {
    "cardinal": car1
  },
  "ssy": {
    "cardinal": car1
  },
  "st": {
    "cardinal": car1
  },
  "su": {
    "cardinal": car7
  },
  "sv": {
    "cardinal": car5,
    "ordinal": ord21
  },
  "sw": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "syr": {
    "cardinal": car1
  },
  "ta": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "te": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "teo": {
    "cardinal": car1
  },
  "th": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "ti": {
    "cardinal": car2
  },
  "tig": {
    "cardinal": car1
  },
  "tk": {
    "cardinal": car1,
    "ordinal": ord22
  },
  "tl": {
    "cardinal": car10,
    "ordinal": ord8
  },
  "tn": {
    "cardinal": car1
  },
  "to": {
    "cardinal": car7
  },
  "tr": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "ts": {
    "cardinal": car1
  },
  "tzm": {
    "cardinal": car36
  },
  "ug": {
    "cardinal": car1
  },
  "uk": {
    "cardinal": car32,
    "ordinal": ord23
  },
  "ur": {
    "cardinal": car5,
    "ordinal": ord1
  },
  "uz": {
    "cardinal": car1,
    "ordinal": ord1
  },
  "ve": {
    "cardinal": car1
  },
  "vi": {
    "cardinal": car7,
    "ordinal": ord8
  },
  "vo": {
    "cardinal": car1
  },
  "vun": {
    "cardinal": car1
  },
  "wa": {
    "cardinal": car2
  },
  "wae": {
    "cardinal": car1
  },
  "wo": {
    "cardinal": car7
  },
  "xh": {
    "cardinal": car1
  },
  "xog": {
    "cardinal": car1
  },
  "yi": {
    "cardinal": car5
  },
  "yo": {
    "cardinal": car7
  },
  "yue": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "zh": {
    "cardinal": car7,
    "ordinal": ord1
  },
  "zu": {
    "cardinal": car3,
    "ordinal": ord1
  }
};