export const PLURAL_ZERO = 'zero';
export const PLURAL_ONE = 'one';
export const PLURAL_TWO = 'two';
export const PLURAL_FEW = 'few';
export const PLURAL_MANY = 'many';
export const PLURAL_OTHER = 'other';
 
export const GENDER_MALE = 'm';
export const GENDER_FEMALE = 'f';
export const GENDER_NEUTRAL = 'n';
 
export const DEFAULT_LOCALE = 'en';

export const NAMESPACE_SEPARATOR = ':';

export const DEFAULT_NAMESPACE = 'default';
export const DEFAULT_KEY_SEPARATOR = '.';
export const DEFAULT_TOKEN_PATTERN = /\{\{([\w.]+)\}\}/g;