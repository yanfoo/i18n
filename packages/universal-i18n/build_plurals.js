/**
This script is used to build lib/plurals.js

This script is part of the npm package "universal-i18n" and
should not be distributed in a non-development environment
as it depends on cldr-data to be installed as a dev-dependency.
*/

const fs = require('fs');


const OP_AND = / and /g;
const OP_OR = / or /g;
const OP_EQUAL = / = /g;
const OP_NOT_EQUAL = / != /g;
const MOD_ALIAS = /(\w) % (\d+)/g;
const EXPR = /(\w\d*?) (!?=) ((?:\d+(\.\.\d+)?,?)+)/g;
const EXPR_RANGE = /(\d+)\.\.(\d+)/;

const DEST_PATH = './src/plurals.js';



/**
rule[locale] => { cardinal: function, ordinal: function }
*/
const rules = {};
const fnDefs = {};
const content = [
   '/**',
   'Plural file generated from CLDR-DATA (http://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html)',
   'See ../build_plurals.js for more information.',
   '**/',
   "import { PLURAL_ZERO, PLURAL_ONE, PLURAL_TWO, PLURAL_FEW, PLURAL_MANY, PLURAL_OTHER } from './const';"
];

/**
Build all cardinal rules from CLDR data
*/
scanPluralData(require('cldr-data/supplemental/plurals')['supplemental']['plurals-type-cardinal'], 'cardinal');
/**
Build all ordinal rules from CLDR data
*/
scanPluralData(require('cldr-data/supplemental/ordinals')['supplemental']['plurals-type-ordinal'], 'ordinal');


const collator = new Intl.Collator(undefined, { numeric: true, sensitivity: 'base' });

const fnDefNames = Object.keys(fnDefs).sort((a, b) => collator.compare(a, b) /*a.localeCompare(b)*/);
const plurals = fnDefNames.reduce((plurals, fnName) => plurals.replace(new RegExp('"' + fnName + '"', 'g'), fnName), JSON.stringify(rules, null, 2));

content.push(fnDefNames.map(name => `${fnDefs[name]};`).join('\n'));
content.push('export default ' + plurals + ';');


fs.writeFileSync(DEST_PATH, content.join('\n\n'));


// --------------------------------------------------------------------


function scanPluralData(pluralData, type) {
   var ruleCount = 1;
   var _cache = {};

   Object.keys(pluralData).forEach(function (locale, index) {
      var defName = type.substr(0, 3) + ruleCount;
      var expr = pluralData[locale];
      var cacheKey = JSON.stringify(expr);

      if (!rules[locale]) {
         rules[locale] = {};
      }

      if (_cache[cacheKey]) {
         rules[locale][type] = _cache[cacheKey];
      } else {
         rules[locale][type] = _cache[cacheKey] = defName;
         fnDefs[defName] = buildRule(expr, defName);

         ruleCount++;
      }
   });
}


function buildRule(cldrData, name) {
   let requireP = false;
   
   const vars = {};
   const defGlobal = (v, arg, fn) => {
      if (arg === 'p') {
         requireP = true;
      } else if (arg === 'n') {
         defN();
      }
      
      vars[v] = `_${v}(${arg})`;
      fnDefs[`_${v}`] = `const _${v} = ${arg} => ${fn}`;
   }
   const defN = () => {
      requireP = true;
      defGlobal('n', 'p', 'Math.abs(p)||0');
   }
      
   const rule = Object.keys(cldrData).map(key => {
      const pluralKey = key.substr(key.lastIndexOf('-') + 1);
      var expr = cldrData[key].substr(0, cldrData[key].indexOf('@')).trim()
         // http://unicode.org/reports/tr35/tr35-numbers.html#Language_Plural_Rules
         .replace('i', () => {
            // i = integer digits of p.
            //vars['i'] = 'Math.floor(n)||0';
            defGlobal('i', 'n', 'Math.floor(n)||0');
            
            return 'i';
         })
         .replace('v', () => {
            // v = number of visible fraction digits in p, with trailing zeros.
            //vars['v'] = '((p+"").split(".")[1]||"").length';
            defGlobal('v', 'p', '((p+"").split(".")[1]||"").length');

            return 'v';
         })
         /*
         --- NOT USED BY CLDR AT THE MOMENT ---
         .replace('w', function () {
           // w = number of visible fraction digits in p, without trailing zeros.
           vars['w'] = '((n+"").split(".")[1]||"").length';
   
           return 'w';
         })
         */
         .replace('f', () => {
            // f = visible fractional digits in p, with trailing zeros.
            //vars['f'] = 'Math.floor((p+"").split(".")[1])||0';
            defGlobal('f', 'p', 'Math.floor((p+"").split(".")[1])||0');
            
            return 'f';
         })
         .replace('t', function () {
            // t = visible fractional digits in p, without trailing zeros.
            //vars['t'] = 'Math.floor((n+"").split(".")[1])||0';
            defGlobal('t', 'n', 'Math.floor((n+"").split(".")[1])||0');

            return 't';
         })
         .replace(MOD_ALIAS, (match, v, mod) => {
            var alias = v + mod;

            if (v === 'n') {
               defN();
            } else if (v === 'p') {
               requireP = true;
            }

            if (!vars[alias]) {
               vars[alias] = match;
            }

            return alias;
         })
         .replace(EXPR, (_match, v, op, n) => {

            // n = 0..2,5  ==> (n === 0 || n === 1 || n === 2 || n === 5)
            // n != 0..2,5 ==> (n !== 0 && n !== 1 && n !== 2 && n !== 5)

            if (v === 'n') {
               defN();
            } else if (v === 'p') {
               requireP = true;
            }

            return n.split(',').map(r => {
               const m = r.match(EXPR_RANGE);
               //var buffer;

               if (m) {
                  //for (buffer = [], m[1] = Math.floor(m[1], 10), m[2] = Math.floor(m[2], 10); m[1] <= m[2]; ++m[1]) {
                  //  buffer.push(v + ' ' + op  + ' ' + m[1]);
                  //}
                  //return buffer.join(' || ');

                  return (op === '=' ? '' : '!') + '(' + v + ' >= ' + m[1] + ' && ' + v + ' <= ' + m[2] + ')';
               } else {
                  return `${v} ${op} ${r}`;
               }
            }).join(op === '=' ? ' || ' : ' && ');
         })
         .replace(OP_AND, ' && ')
         .replace(OP_OR, ' || ')
         .replace(OP_EQUAL, ' === ')
         .replace(OP_NOT_EQUAL, ' !== ')
         ;

      if (!expr) {
         expr = 'PLURAL_' + pluralKey.toUpperCase();
      } else {
         expr = expr + ' ? PLURAL_' + pluralKey.toUpperCase();
      }

      return expr;
   }).join(' : ');
   
   const varDecl = Object.keys(vars).map(v => v + ' = ' + vars[v]).join(', ');

   return 'const ' + name + ' = ' + (requireP ? 'p' : '()') + ' => { ' + (varDecl && ('const ' + varDecl + '; ')) + 'return ' + rule + '; }';
}