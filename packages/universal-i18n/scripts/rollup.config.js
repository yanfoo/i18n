import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import replace from 'rollup-plugin-replace';
import nodeGlobals from 'rollup-plugin-node-globals';
import { terser } from 'rollup-plugin-terser';
import { sizeSnapshot } from 'rollup-plugin-size-snapshot';

const input = './src/index.js';
const globals = {
  'universal-i18n': 'i18n',
};
const resolveOptions = {
  extensions: [ '.mjs', '.js', '.jsx', '.json' ]
};
const babelOptions = {
  exclude: ["./build/**", "node_modules/**"],
  // We are using @babel/plugin-transform-runtime
  runtimeHelpers: true,
  configFile: './babel.config.js',
};
const commonjsOptions = {
  //ignoreGlobal: true,
  //include: ["./src/index.js", "node_modules/**"],
  //namedExports: {},
};

export default [
  {
    input,
    output: {
      file: 'build/umd/universal-i18n.js',
      format: 'umd',
      name: 'i18n',
      exports: 'named',
      //sourcemap: true,
      globals,
    },
    external: Object.keys(globals),
    plugins: [
      replace({ 'process.env.NODE_ENV': JSON.stringify('development') }),
      babel(babelOptions),
      nodeResolve(resolveOptions),
      commonjs(commonjsOptions),
      nodeGlobals(), // Wait for https://github.com/cssinjs/jss/pull/893
    ],
  },
  {
    input,
    output: {
      file: 'build/umd/universal-i18n.min.js',
      format: 'umd',
      name: 'i18n',
      exports: 'named',
      sourcemap: true,
      globals,
    },
    external: Object.keys(globals),
    plugins: [
      replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
      babel(babelOptions),
      nodeResolve(resolveOptions),
      commonjs(commonjsOptions),
      nodeGlobals(), // Wait for https://github.com/cssinjs/jss/pull/893
      sizeSnapshot({ snapshotPath: 'size-snapshot.json' }),
      terser(),
    ],
  },
];